Ext.define('CodingExercise.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar'
    ],

    config: {
    	
    	fullscreen:		true,
        tabBarPosition: 'bottom',
        items: [{
        	
        	title: 'Home',
        	iconCls: 'home',
        	xtype:	'home'
        
        },{
        	
        	title: 'Account',
        	iconCls: 'user',
        	xtype:	'account'

        },{

        	title: 'Help',
        	iconCls: 'help',
        	disabled: true
        	
        },{
        	
        	title: 'Settings',
        	iconCls: 'settings',
        	xtype: 'settings'
        	
        }]
    }
});
