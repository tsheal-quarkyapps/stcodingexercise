Ext.define('CodingExercise.view.BaseView',{
	
	extend:		'Ext.Container',
	fullscreen:	true,
	xtype:		'baseview',
		
	config:	{

		content:			null,
		backButtonHidden:	false,
		
		layout:		'vbox',
		bubbleEvents: ['backbuttontap'],
		items:	[{
			
			xtype:	'titlebar',
			title:	'<div class="logo"/>',
			bubbleEvents: ['backbuttontap'],
			items:	[{ 
				
				xtype:		'button',
				ui:			'back',
				text:		'Back',
				action:		'back'
					
			}]
		
		},{ xtype:	'userinfo' },{
			
			xtype:	'container',
			style:	'background-color: gray',
			layout:	'vbox',
			flex:	1,
			width:	'100%',
			action:	'content'
			
		}]

	},
	
	updateBackButtonHidden:	function (newValue) {
		
		this.down('button[action=back]').setHidden(newValue);
		
	},
	
	updateContent:	function (newContent) {
		
		if (!newContent)
			return;
		this.down('container[action=content]').setItems([newContent]);
		
	},
	
	getBackButton:	function () {
		
		return this.down('button[action=back');
		
	},
	
	
	constructor:	function () {
		
		console.log('constructor');
		this.callParent(arguments);
		
		var me = this;
	}
	
});