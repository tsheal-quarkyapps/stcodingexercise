Ext.define('CodingExercise.view.Settings', {
	
	extend:		'CodingExercise.view.BaseView',
	xtype:		'settings',
	fullscreen: true,
	
	config:		{
		
	     displayField: 'text',
	     OnItemDisclosure: true,
	     
	     content:	{
	    	
	    	 xtype:		'formpanel',
	    	 width:		'100%',
	    	 padding:	10,
	    	 flex:		1,
	    	 items:		[{

	    		 xtype: 'fieldset',
	    		 items:	[{

	    			 xtype:	'selectfield',
	    			 name: 	'country',
	    			 options: [
	    			           {text: 'United States',  	value: 'US'},
	    			           {text: 'Canada', 			value: 'Canada'}]

	    		 },{

	    			 xtype:			'emailfield',
	    			 name:			'email',
	    			 placeHolder:	'Email'

	    		 },{

	    			 xtype:			'numberfield',
	    			 name:			'phone',
	    			 placeHolder:	'Phone'

	    		 }]

	    	 },{

    			 xtype:			'button',
    			 cls:			'form-button',
    			 text:			'Update',
    			 action:		'update',
    			 margin:		'10 0 10 0'

    		 }]

	     }

	},
	
	initialize:	function () {
		
		// Check to see if we have any data stored
		var settingsStore = Ext.getStore('Settings');
		var me = this;
		
		settingsStore.load({
			
			callback:	function () {
				
				var count = settingsStore.getCount();
				if (count > 0) {
					
					var values = settingsStore.getAt(0).getData();
					me.down('formpanel').setValues(values);
					
				} else {
					
					var defaultStore = Ext.getStore('DefaultSettings');
					defaultStore.load({
						
						callback:	function () {
							
							var values = defaultStore.getAt(0).getData();
							me.down('formpanel').setValues(values);
							
						}
						
					});
					
				}
				
				console.log('Settings Store Count: ' + settingsStore.getCount());
				
			}
			
		});
		
	}

});