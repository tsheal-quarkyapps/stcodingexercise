Ext.define('CodingExercise.view.UserInfo', {
	
	extend: 'Ext.Container',
	xtype:	'userinfo',
	
	config:	{
		
		cls: 	'user-info',
		layout:	'hbox',
		items: [{
			
			xtype:	'container',
			name:	'username',
			width:	'auto',
			html:	'Username',
			margin:	'10'
			
		},{
			
			xtype:	'container',
			name:	'balance',
			flex:	1,
			style:	'text-align: right',
			html:	'Balance',
			margin:	'10'
			
		}]
			
	}
	
})