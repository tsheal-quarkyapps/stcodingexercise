Ext.define('CodingExercise.view.Account', {
	
	extend:		'CodingExercise.view.BaseView',
	xtype:		'account',
	fullscreen: true,
	
	config:		{
		
	     displayField: 'text',
	     OnItemDisclosure: true,
	     
	     content:	{
	    	
	    	 xtype:	'container',
	    	 flex:	1,
	    	 width:	'100%',
	    	 layout:'vbox',
	    	 items: [{
	    		
	    		xtype:	'titlebar',
	    		action:	'listTitle',
	    		title:	'Account'
	    		 
	    	 },{
	    		 
	    		 xtype:		'panel',
	    		 flex:		1,
	    		 width:		'100%',
	    		 layout:	{
	    			 
	    			 type:	'card',
		    		 animation:	{
		    			
		    			 type:		'slide',
		    			 direction:	'left'
		    			 
		    		 }
	    		 },
	    		 items:		[{
	    			 
	    			 xtype:				'list',
	    			 itemTpl:			'{text}',
	    			 store:				'MenuItems',
	    			 onItemDisclosure:	true
	    			 
	    		 }]
	    	 	    		 
	    	 }]
	    	 
	     }

	}

});