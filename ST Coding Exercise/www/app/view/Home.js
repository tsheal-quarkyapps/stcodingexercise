Ext.define('CodingExercise.view.Home', {
	
	extend: 	'CodingExercise.view.BaseView',
	xtype:		'home',
	fullscreen:	true,
	
	config:	{

		backButtonHidden:	true,
		content:	{
			
			xtype:		'carousel',
			width:		'100%',
			flex:		1,
			items:	[{

				xtype:	'container',
				layout:	'fit',
				items:	[{
					xtype:	'image',
					src:	'images/a380.jpg',
					margin:	'20px'
				}]

			},{

				xtype:	'container',
				layout:	'fit',
				items:	[{
					xtype:	'image',
					src:	'images/a350.jpg',
					margin:	'20px'
				}]
				
			},{

				xtype:	'container',
				layout:	'fit',
				items:	[{
					xtype:	'image',
					src:	'images/a340.jpg',
					margin:	'20px'
				}]
				
			}]
			
		}
		
	}
		
	
})