Ext.define('CodingExercise.store.Settings', {

	extend:	'Ext.data.Store',
	requires:	['Ext.data.proxy.LocalStorage'],
	config:	{
		model: 'CodingExercise.model.Settings',
		proxy: {
            type: 'localstorage',
            id  : 'settings'
        }
	}
});