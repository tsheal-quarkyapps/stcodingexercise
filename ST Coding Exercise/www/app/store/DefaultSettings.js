Ext.define('CodingExercise.store.DefaultSettings', {

	extend:	'Ext.data.Store',
	config:	{
		model: 'CodingExercise.model.Settings',
		proxy: {
			type: 'ajax',
			url : 'json/settings.json'
		}
	}
});