Ext.define('CodingExercise.store.MenuItems', {

	extend:	'Ext.data.Store',
	config:	{
		model: 'CodingExercise.model.MenuItem',
		
		proxy: {
			type: 'ajax',
			url : 'json/menu.json',
			reader:	{
				
				type:			'json',
				rootProperty:	'items'
			}
		},
		autoload: true
		
	}
});
