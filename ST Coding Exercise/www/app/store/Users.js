Ext.define('CodingExercise.store.Users', {

	extend:	'Ext.data.Store',
	config:	{
		model: 'CodingExercise.model.User',
		proxy: {
			type: 'ajax',
			url : 'json/userInfo.json'
		}
	}
});