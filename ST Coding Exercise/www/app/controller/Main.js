Ext.define('CodingExercise.controller.Main', {
	
    extend: 'Ext.app.Controller',
    
    config: {
        refs: {
        	
        },
        control: {
            
        }
    },
    
    users:	null,
    username:	'',
    balance:	'',
    
    
    //called when the Application is launched, remove if not needed
    launch: function(app) {
        
    	users = Ext.getStore('Users');
    	this.updateUserInfo();
    	
    	var me = this;
    	setInterval(function() {
    		
    		me.updateUserInfo();
    		
    	}, 10000);
    	
    },
    
    updateUserInfo: function() {
    	
    	var me = this;
    	users.load({
    		
    		callback: function (records, operation, success) {

    			if (success) {
    				
    				var record = records[0].getData(true);
    				
    				// Get a collection of all User Info Bars and apply the data to them.
    				var userInfoViews = Ext.ComponentQuery.query('userinfo');
					me.username = record['username']
					me.balance = 'US$ ' + record['balance'];
					
    				Ext.Array.each(userInfoViews, function(userInfoView) {
    					
    					userInfoView.down('container[name=username]').setHtml(me.username);
    					userInfoView.down('container[name=balance]').setHtml(me.balance);
    				    
    				});
    				
    			}
    	    	
    		}
    		
    	});
    	
    }
    
});
