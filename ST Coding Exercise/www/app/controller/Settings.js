Ext.define('CodingExercise.controller.Settings', {
    extend: 'Ext.app.Controller',
    
    config: {
        refs: {
            
        	settings:		'settings',
        	backButton:		'settings button[action=back]',
        	updateButton:	'settings button[action=update]'
            
        },
        control: {
            
        	backButton:	{ tap: 'backButtonTapped' },
        	updateButton: { tap: 'updateButtonTapped' }
        
        }
    },
    
    //called when the Application is launched, remove if not needed
    launch: function(app) {
        
    },
    
    backButtonTapped:	function (button, e, eOpts) {

    	var settings = button.up('settings');
    	
    	// Go Home
    	var tabpanel = settings.up('main');
    	tabpanel.setActiveItem(0);
    		
    },
    
    updateButtonTapped:	function (button, e, eOpts) {
    	
    	var settings = button.up('settings');
    	
    	settings.down('field[name=email]').removeCls('invalid-field');
    	settings.down('field[name=phone]').removeCls('invalid-field');
    	
    	var formData = settings.down('formpanel').getValues();
    	var formModel = Ext.create('CodingExercise.model.Settings');
    	formModel.set(formData);
    	
    	var errors = formModel.validate();
    	if (!errors.isValid()) {
    		
    		errors.each( function (error) {
    			
    			settings.down('field[name=' + error.getField() + ']').addCls('invalid-field');
    			
    		});
    		
    	} else {
    		
    		var settingsStore = Ext.getStore('Settings');
    		settingsStore.load();
    		settingsStore.removeAll();
    		settingsStore.sync();
    		
    		settingsStore.add(formModel);
    		settingsStore.sync();
    		
    		Ext.Msg.alert('Settings Updated', 'Your information has been successfully updated.');
    		
    	}
    	
    }
    
});
