Ext.define('CodingExercise.controller.Account', {
    extend: 'Ext.app.Controller',
    
    config: {
        refs: {
            
        	account:	'account',
        	backButton:	'account button[action=back]',
        	list:		'account list'
        	
        },
        control: {
            
        	backButton:	{ tap:	'backButtonTapped' },
        	list:		{ itemtap: 'listItemTapped'}
        	
        }
    },
    
    accountSelectionChanged: function (account, list, selections, eOpts) {
    	
    	account.getBackButton().setHidden(true);
    	
    },
    
    backButtonTapped:	function (button, e, eOpts) {
    	
    	var account = button.up('account');
    	if (!account)
    		return;
    	
    	// Get stack count
    	var listStack = account.down('panel');
    	var stackCount = listStack.getItems().getCount();
    	
    	if (stackCount < 2) {
    		
    		// Go Home
    		var tabpanel = account.up('main');
    		tabpanel.setActiveItem(0);
    		
    	} else {
    		
    		//Go back one in the list & set the title
    		listStack.getLayout().getAnimation().setDirection('right');
    		listStack.setActiveItem(stackCount - 2);
    		listStack.getLayout().getAnimation().setDirection('left');
    		setTimeout(function() {
    			
    			listStack.removeAt(stackCount - 1);
        		
        		stackCount = listStack.getItems().getCount();
        		console.log(stackCount);
        		
        		var title = stackCount < 2 ? 'Account' : listStack.getActiveItem()['title'];
        		console.log(title);
        		account.down('titlebar[action=listTitle]').setTitle(title);
    			
    		}, 200);
    		
    	}
    	
    },
    
    listItemTapped:	function (list, index, target, record, e, eOpts) {
    	
    	console.log(record);
    	
    	// Get reference to account object
    	var account = list.up('account');
    	setTimeout(function () {

        	list.deselect(index);
    		
    	}, 500);
    	
    	// If there is an items object push a new list onto the stack
    	if (record.get('items')) {
    		
    		account.down('titlebar[action=listTitle]').setTitle(record.get('text'));
    		
    		var newList = Ext.create('Ext.dataview.List', {

    			itemTpl:			'{text}',
   			 	data:				record.get('items'),
   			 	onItemDisclosure:	true,
   			 	title:				record.get('text')
    			
    		});
    		
    		var listStack = account.down('panel');
    		listStack.add(newList);
    		
    		var stackCount = listStack.getItems().getCount();
    		listStack.setActiveItem(stackCount - 1);
    		
    	} else if (record.get('content')) {
    		
    		account.down('titlebar[action=listTitle]').setTitle(record.get('text'));
    		var url = 'html/' + record.get('content');
    		
    		Ext.Ajax.request({
    			
                url : url,
                method : "GET",
                success : function( response, request ) {
                	
                	var newContainer = Ext.create('Ext.Container', {
            			
            			styleHtmlContent:	true,
            			html:				response.responseText,
            			title:				record.get('text'),
            			scrollable:			true
            			
            		});
            		
            		var listStack = account.down('panel');
            		listStack.add(newContainer);
            		
            		var stackCount = listStack.getItems().getCount();
            		listStack.setActiveItem(stackCount - 1);
            		
                }
    		
    		});
    		
    	}
    	
    }
});
