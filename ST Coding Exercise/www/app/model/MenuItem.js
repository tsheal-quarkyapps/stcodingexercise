Ext.define('CodingExercise.model.MenuItem', {

	extend: 'Ext.data.Model',
	config: {

		fields: [{ name: 'text', type: 'string' },
		         { name: 'items', type: 'object'},
		         { name: 'content', type: 'string'}]
	}
});
