Ext.define('CodingExercise.model.Settings', {

	extend: 'Ext.data.Model',
	config: {

		idProperty:	'id',
		identifier:	'uuid',
		fields: [{
			
			name: 'id', type: 'string'
			
		},{ 
			
			name: 'country', type: 'string' 
				
		},{ 
			
			name: 'email', type: 'string' 
				
		},{ 
				
			name: 'phone', type: 'string' 
				
		}],
		         
		validations: [{

			type: 'format',
			field: 'email',
			matcher: /^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/,
			message: 'Incorrect email address format'
				
		},{
			
			type:		'format',
			field:		'phone',
			matcher:	/^\d{10}$/,
			message:	'10 digit phone numbers only'
			
		}]

	}

});
