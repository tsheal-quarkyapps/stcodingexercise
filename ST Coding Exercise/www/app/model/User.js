Ext.define('CodingExercise.model.User', {

	extend: 'Ext.data.Model',
	config: {

		fields: [{ name: 'username', type: 'string' },
		         { name: 'balance', type: 'double' }]
	}
});
